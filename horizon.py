# -*- coding: utf-8 -*-

from . import curvature
from . import diff
from . import interp
from . import utils

import enum
import itertools as it
from functools import cached_property, wraps
import sys

import numpy as np
import scipy.integrate
from scipy.optimize import root_scalar

class IntegrationFailed(Exception):
    pass
class NoBracketFound(Exception):
    pass

def _tensor_transform_cart_spherical_d2(theta, r, x, z, tens):
    zero = np.zeros_like(theta)
    st   = np.sin(theta)
    ct   = np.cos(theta)
    jac  = np.array([[st,    r * ct,   zero ],
                     [zero,    zero, r * st ],
                     [ct,   -r * st,   zero ]])
    return np.einsum('ai...,bj...,ab...->ij...', jac, jac, tens)

def _tensor_transform_cart_spherical_u2(theta, r, x, z, tens):
    eps = 1e-10 # small number to replace NaNs with just huge numbers
    x = np.where(np.abs(x) > eps, x, eps)

    zero = np.zeros_like(theta)
    st = np.sin(theta)
    ct = np.cos(theta)
    r2 = r * r
    # ∂ spherical / ∂ cart
    jac_inv = np.array([[st,       zero,      ct ],
                        [z / r2,   zero, -x / r2 ],
                        [zero,   1. / x,     zero ]])
    return np.einsum('ai...,bj...,ij...->ab...', jac_inv, jac_inv, tens)

def _dtensor_transform_cart_spherical_d2(theta, r, x, z, tens, dtens):
    zero = np.zeros_like(theta)
    st = np.sin(theta)
    ct = np.cos(theta)
    jac = np.array([[st,    r * ct,   zero ],
                    [zero,    zero, r * st ],
                    [ct,   -r * st,   zero ]])
    djac = np.array([[ # ∂^2 x
                        [ zero, ct,    zero ], # / ∂r
                        [ ct, -r * st, zero ], # / ∂θ
                        [ zero, zero,    -x ], # / ∂φ
                     ],
                     [ # ∂^2 y
                         [ zero, zero,     st ], # / ∂r
                         [ zero, zero, r * ct ], # / ∂θ
                         [ st, r * ct, zero   ], # / ∂φ
                     ],
                     [ # ∂^2 z
                         [ zero,     -st, zero ], # / ∂r
                         [ -st,  -r * ct, zero ], # / ∂θ
                         [ zero,    zero, zero ], # / ∂φ
                     ]])

    return np.einsum('bij...,ck...,bc...->ijk...', djac, jac, tens) +\
           np.einsum('bj...,cik...,bc...->ijk...', jac, djac, tens) +\
           np.einsum('bj...,ck...,ai...,abc...->ijk...', jac, jac, jac, dtens)

def _christoffel_transform_cart_spherical(theta, r, x, z, Gamma_cart):
    eps = 1e-10 # small number to replace NaNs with just huge numbers
    x = np.where(np.abs(x) > eps, x, eps)

    zero = np.zeros_like(theta)
    st = np.sin(theta)
    ct = np.cos(theta)
    r2 = r * r
    # ∂ cart / ∂ spherical
    jac = np.array([[st,    r * ct,   zero ],
                    [zero,    zero, r * st ],
                    [ct,   -r * st,   zero ]])
    # ∂ spherical / ∂ cart
    jac_inv = np.array([[st,       zero,      ct ],
                        [z / r2,   zero, -x / r2 ],
                        [zero,   1. / x,     zero ]])
    djac = np.array([[ # ∂^2 x
                        [ zero, ct,    zero ], # / ∂r
                        [ ct, -r * st, zero ], # / ∂θ
                        [ zero, zero,    -x ], # / ∂φ
                     ],
                     [ # ∂^2 y
                         [ zero, zero,     st ], # / ∂r
                         [ zero, zero, r * ct ], # / ∂θ
                         [ st, r * ct, zero   ], # / ∂φ
                     ],
                     [ # ∂^2 z
                         [ zero,     -st, zero ], # / ∂r
                         [ -st,  -r * ct, zero ], # / ∂θ
                         [ zero,    zero, zero ], # / ∂φ
                     ]])

    return np.einsum('ai...,jb...,kc...,ijk...->abc...', jac_inv, jac, jac, Gamma_cart) +\
           np.einsum('ak...,kbc...->abc...', jac_inv, djac)

def _dchristoffel_transform_cart_spherical(theta, r, x, z, Gamma_cart, dGamma_cart):
    eps = 1e-10 # small number to replace NaNs with just huge numbers
    x = np.where(np.abs(x) > eps, x, eps)

    zero = np.zeros_like(theta)
    st = np.sin(theta)
    ct = np.cos(theta)

    r2 = r * r
    r3 = r2 * r
    r4 = r3 * r

    x2 = x * x
    x4 = x2 * x2

    z2 = z * z
    # ∂ cart / ∂ spherical
    jac = np.array([[st,    r * ct,   zero ],
                    [zero,    zero, r * st ],
                    [ct,   -r * st,   zero ]])
    # ∂ spherical / ∂ cart
    jac_inv = np.array([[st,       zero,      ct ],
                        [z / r2,   zero, -x / r2 ],
                        [zero,   1. / x,     zero ]])
    djac = np.array([[ # ∂^2 x
                        [ zero, ct,    zero ], # / ∂r
                        [ ct, -r * st, zero ], # / ∂θ
                        [ zero, zero,    -x ], # / ∂φ
                     ],
                     [ # ∂^2 y
                         [ zero, zero,     st ], # / ∂r
                         [ zero, zero, r * ct ], # / ∂θ
                         [ st, r * ct, zero   ], # / ∂φ
                     ],
                     [ # ∂^2 z
                         [ zero,     -st, zero ], # / ∂r
                         [ -st,  -r * ct, zero ], # / ∂θ
                         [ zero,    zero, zero ], # / ∂φ
                     ]])
    djac_inv = np.array([[ # ∂^2 r
                             [ z2 / r3,     zero,   -x * z / r3 ], # / ∂x
                             [ zero,      1. / r,          zero ], # / ∂y
                             [ -x * z / r3, zero,       x2 / r3 ], # / ∂z
                         ],
                         [ # ∂^2 θ
                             [     -2 * z * np.abs(x) / r4,                                       zero, np.sign(x) * (x2 - z2) / r4], # / ∂x
                             [                        zero, z * (x4 + x2 * z2) * np.abs(x) / (r4 * x4),                       zero ], # / ∂y
                             [ np.sign(x) * (x2 - z2) / r4,                                       zero,   2 * z * np.abs(x) / r4   ], # / ∂z
                         ],
                         [ # ∂^2 φ
                             [ zero,     -1 / x2, zero ], # / ∂x
                             [ -1 / x2,     zero, zero ], # / ∂y
                             [ zero,        zero, zero ], # / ∂z
                         ]])

    d2jac = np.array([[ # ∂^3 x
                         [ # / ∂r
                             [ zero,   zero,    zero ], # / ∂r
                             [ zero,    -st,    zero ], # / ∂θ
                             [ zero,   zero,     -st ], # / ∂φ
                         ],
                         [ # / ∂θ
                             [ zero,    -st,    zero ], # / ∂r
                             [  -st,    -ct,    zero ], # / ∂θ
                             [ zero,   zero, -r * ct ], # / ∂φ
                         ],
                         [ # / ∂φ
                             [ zero,   zero,     -st ], # / ∂r
                             [ zero,   zero, -r * ct ], # / ∂θ
                             [ -st, -r * ct,    zero ], # / ∂φ
                         ],
                      ],
                      [ # ∂^3 y
                         [ # / ∂r
                             [ zero,   zero,    zero ], # / ∂r
                             [ zero,   zero,      ct ], # / ∂θ
                             [ zero,     ct,    zero ], # / ∂φ
                         ],
                         [ # / ∂θ
                             [ zero,   zero,      ct ], # / ∂r
                             [ zero,   zero, -r * st ], # / ∂θ
                             [  ct, -r * st,    zero ], # / ∂φ
                         ],
                         [ # / ∂φ
                             [ zero,     ct,    zero ], # / ∂r
                             [  ct, -r * st,    zero ], # / ∂θ
                             [ zero,   zero, -r * st ], # / ∂φ
                         ],
                      ],
                      [ # ∂^3 z
                         [ # / ∂r
                             [ zero,   zero,    zero ], # / ∂r
                             [ zero,    -ct,    zero ], # / ∂θ
                             [ zero,   zero,    zero ], # / ∂φ
                         ],
                         [ # / ∂θ
                             [ zero,    -ct,    zero ], # / ∂r
                             [  -ct, r * st,    zero ], # / ∂θ
                             [ zero,   zero,    zero ], # / ∂φ
                         ],
                         [ # / ∂φ
                             [ zero,   zero,    zero ], # / ∂r
                             [ zero,   zero,    zero ], # / ∂θ
                             [ zero,   zero,    zero ], # / ∂φ
                         ],
                      ]])

    for i, j, k, l in it.product(range(3), repeat = 4):
        assert(np.all(d2jac[i, j, k, l] == d2jac[i, l, j, k]))
        assert(np.all(d2jac[i, j, k, l] == d2jac[i, k, l, j]))
        assert(np.all(d2jac[i, j, k, l] == d2jac[i, l, k, j]))

    # this is broken (probably borked index somewhere)
    #return np.einsum('ia...,bj...,ck...,dl...,dabc...->lijk...', jac_inv,  jac, jac,  jac, dGamma_cart) +\
    #       np.einsum('iad...,dl...,bj...,ck...,abc...->lijk...', djac_inv, jac, jac,  jac,  Gamma_cart) +\
    #       np.einsum('ia...,bjl...,ck...,abc...->lijk...',       jac_inv,      djac,  jac,  Gamma_cart) +\
    #       np.einsum('ia...,bj...,ckl...,abc...->lijk...',       jac_inv,       jac, djac,  Gamma_cart) +\
    #       np.einsum('ajkl...,ia...->lijk...', d2jac, jac_inv)                                          +\
    #       np.einsum('ajk...,iab...,bl...->lijk...', djac, djac_inv, jac_inv)

    ret = np.empty((3,) + Gamma_cart.shape)
    for a, b, c, d in it.product(range(3), repeat = 4):
        val = np.zeros_like(theta)
        for i, j, k, l in it.product(range(3), repeat = 4):
            val += jac[l, d] * jac_inv[a, i] * jac[j, b] * jac[k, c] * dGamma_cart[l, i, j, k]

        for i, j, k in it.product(range(3), repeat = 3):
            val += (np.einsum('i...,i...', djac_inv[a, i], jac[:, d]) * jac[j, b] * jac[k, c] +
                    jac_inv[a, i] * djac[j, b, d] * jac[k, c] +
                    jac_inv[a, i] * jac[j, b] * djac[k, c, d]) * Gamma_cart[i, j, k]

        for i, j in it.product(range(3), repeat = 2):
            val += djac[i, b, c] * djac_inv[a, i, j] * jac[j, d]

        for i in range(3):
            val += d2jac[i, b, c, d] * jac_inv[a, i]

        ret[d, a, b, c] = val

    return ret

class _GridArray2D(np.ndarray):
    """
    An N-dimensional array sampled on a uniform cartesian coordinate grid.
    Can be treated as a N+2-dimensional ndarray (last two indices are spatial)
    or interpolated. Slicing the non-spatial indices preserves the ability to
    interpolate.
    """
    _X          = None
    _Z          = None
    _data       = None

    _interp     = None

    def __new__(cls, X, Z, data, interp_order):
        obj = np.asarray(data).view(cls)

        obj._X              = X
        obj._Z              = Z
        obj._interp_order   = interp_order
        obj._orig_type      = data.__class__

        origin = (Z[0, 0], X[0, 0])
        step   = (Z[1, 0] - Z[0, 0], X[0, 1] - X[0, 0])

        obj._interp = np.empty(data.shape[:-2], dtype = np.object)

        for idx in np.ndindex(obj._interp.shape):
            obj._interp[idx] = interp.Interp2D_C(origin, step, data[idx], interp_order)

        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return

        for field in '_X', '_Z', '_interp_order', '_interp':
            setattr(self, field, getattr(obj, field, None))

    def __getitem__(self, key):
        ret = super().__getitem__(key)

        # if we are slicing only the non-spatial indices, slice
        # the interpolators on the returned object as well
        if (isinstance(key, slice) or
            (isinstance(key, tuple) and len(key) <= len(self._interp.shape))):
            ret._interp = self._interp[key]
            return ret

        # otherwise return a plain ndarray with interpolation capabilities
        return ret.view(np.ndarray)

    def interp(self, x, z):
        if isinstance(self._interp, np.ndarray):
            assert(self._interp.shape == self.shape[:-2])

            ret = np.empty(self._interp.shape + np.asarray(x).shape)
            for idx in np.ndindex(self._interp.shape):
                ret[idx] = self._interp[idx](z, x)
            return ret

        assert(self.shape[:-2] == ())
        return self._interp(z, x)


def _grid_array(method):
    """
    Decorator for AHCalc methods that wraps the result in _GridArray2D.
    """

    @wraps(method)
    def wrapper(self):
        return _GridArray2D(self.X, self.Z, method(self), self._interp_order)

    return wrapper

@enum.unique
class ShootResult(enum.Enum):
    EXPIRED    = enum.auto()
    'Integration reached upper bound on the curve parameter λ'

    BOUNDARY   = enum.auto()
    'Integration reached outer grid boundary'

    AXIS       = enum.auto()
    'Integration hit the axis of symmetry'

    AXIS_REPEL = enum.auto()
    'Integration was "repelled" by the axis with terminate_axis_repel=True'

class ShootSolution:
    result = None
    sol    = None

    def __init__(self, result, sol):
        self.result = result
        self.sol    = sol

    def __call__(self, N):
        t = np.linspace(self.sol.t_min, self.sol.t_max, N)
        return self.sol(t)

class AHCalc(object):
    """
    Object encapsulating an axisymmetric apparent horizon calculation, intended
    to be used with the nonlin_solve_1d solver.
    """

    """
    A list of callables calculating the right-hand side of the axisymmetric AH
    equation and its variational derivatives.
    Intended to be passed to nonlin_solve_1d.
    """
    Fs = None

    _diff_op        = None
    _diff2_op       = None
    _interp_order   = None

    _X              = None
    _Z              = None
    _metric_cart    = None
    _curv_cart      = None

    class _RHSFunc:
        _hc = None

        def __init__(self, hc):
            self._hc = hc

        def __call__(self, val = 0.0):
            rhs      = lambda x, y: self._hc._ah_rhs(x, y, val = val)
            rhs_var0 = lambda x, y: self._hc._var_diff(x, y, rhs, 0)
            rhs_var1 = lambda x, y: self._hc._var_diff(x, y, rhs, 1)

            return (rhs, rhs_var0, rhs_var1)

        def __getitem__(self, key):
            return self()[key]

        def __len__(self):
            return len(self())

        def __iter__(self):
            return iter(self())

    def __init__(self, X, Z, metric_cart, curv_cart, *,
                 interp_order = 6):
        self._diff_op      = diff.fd8
        self._diff2_op     = diff.fd28
        self._interp_order = interp_order

        self._X            = X
        self._Z            = Z
        self._metric_cart  = _GridArray2D(X, Z, metric_cart, self._interp_order)
        self._curv_cart    = _GridArray2D(X, Z, curv_cart,   self._interp_order)

        self.Fs = [self._ah_rhs, self._dF_r_fd, self._dF_R_fd]
        self.rhs           = self._RHSFunc(self)

    @property
    def X(self):
        return self._X
    @property
    def Z(self):
        return self._Z
    @property
    def metric_cart(self):
        return self._metric_cart
    @property
    def curv_cart(self):
        return self._curv_cart

    @cached_property
    @_grid_array
    def metric_u_cart(self):
        return np.ascontiguousarray(utils.matrix_invert(self.metric_cart))
    @cached_property
    @_grid_array
    def dmetric_cart(self):
        return curvature._calc_dmetric(self.X, self.Z, self.metric_cart, self._diff_op)
    @cached_property
    @_grid_array
    def d2metric_cart(self):
        return curvature._calc_d2metric(self.X, self.Z, self.metric_cart,
                                        self.dmetric_cart, self._diff_op, self._diff2_op)
    @cached_property
    @_grid_array
    def dmetric_u_cart(self):
        return -np.einsum('ij...,km...,ljk...->lim...',
                          self.metric_u_cart, self.metric_u_cart,
                          self.dmetric_cart)
    @cached_property
    @_grid_array
    def Gamma_cart(self):
        return curvature._calc_christoffel(self.metric_cart, self.metric_u_cart,
                                           self.dmetric_cart)
    @cached_property
    @_grid_array
    def dGamma_cart(self):
        return curvature._calc_dchristoffel(self.metric_cart,  self.metric_u_cart,
                                            self.dmetric_cart, self.dmetric_u_cart,
                                            self.d2metric_cart)
    @cached_property
    @_grid_array
    def dcurv_cart(self):
        return curvature._calc_dmetric(self.X, self.Z, self.curv_cart, self._diff_op)

    def _spherical_metric(self, theta, r, x, z):
        metric_cart_dst = self.metric_cart.interp(x, z)
        return _tensor_transform_cart_spherical_d2(theta, r, x, z, metric_cart_dst)

    def _spherical_dmetric(self, theta, r, x, z, metric, Gamma):
        # extract the derivatives of the metric from the Christoffel symbols
        Gamma_l = np.einsum('ij...,ikl...->jkl...', metric, Gamma)
        dmetric = np.empty_like(Gamma_l)
        for i, j, k in it.product(range(3), repeat = 3):
            dmetric[i, j, k] = Gamma_l[j, i, k] + Gamma_l[k, i, j]

        return dmetric

    def _spherical_metric_u(self, theta, r, x, z):
        metric_u_cart_dst = self.metric_u_cart.interp(x, z)
        return _tensor_transform_cart_spherical_u2(theta, r, x, z, metric_u_cart_dst)

    def _spherical_dmetric_u(self, theta, r, x, z, dmetric, metric_u):
        return -np.einsum('ik...,jl...,mkl...->mij...', metric_u, metric_u, dmetric)

    def _spherical_curv(self, theta, r, x, z):
        curv_cart_dst = self.curv_cart.interp(x, z)
        return _tensor_transform_cart_spherical_d2(theta, r, x, z, curv_cart_dst)

    def _spherical_dcurv(self, theta, r, x, z):
        curv_cart_dst = self.curv_cart.interp(x, z)
        dcurv_cart_dst = self.dcurv_cart.interp(x, z)
        return _dtensor_transform_cart_spherical_d2(theta, r, x, z, curv_cart_dst, dcurv_cart_dst)

    def _spherical_Gamma(self, theta, r, x, z):
        Gamma_cart_dst = self.Gamma_cart.interp(x, z)
        return _christoffel_transform_cart_spherical(theta, r, x, z, Gamma_cart_dst)

    def _spherical_dGamma(self, theta, r, x, z):
        Gamma_cart_dst  = self. Gamma_cart.interp(x, z)
        dGamma_cart_dst = self.dGamma_cart.interp(x, z)

        return _dchristoffel_transform_cart_spherical(theta, r, x, z, Gamma_cart_dst, dGamma_cart_dst)

    def _spherical_trK(self, theta, r, x, z):
        metric_u = self.metric_u_cart.interp(x, z)
        curv     = self.curv_cart.    interp(x, z)

        return np.einsum('ij...,ij...', metric_u, curv)

    def _var_diff(self, x, y, func, idx, eps = 1e-8):
        y_var      = list(y)
        y_var[idx] = y[idx] + eps
        f_plus      = func(x, y_var)

        y_var[idx]  = y[idx] - eps
        f_minus      = func(x, y_var)

        return (f_plus - f_minus) / (2 * eps)

    def _ah_rhs(self, theta, H, val = 0.0):
        r, dr = H

        r2 = r * r
        st = np.sin(theta)
        ct = np.cos(theta)

        x = r * np.sin(theta)
        z = r * np.cos(theta)

        axis = np.abs(theta / np.pi - (theta / np.pi).astype(int)) < 1e-12

        metric   = self._spherical_metric   (theta, r, x, z)
        metric_u = self._spherical_metric_u(theta, r, x, z)
        curv     = self._spherical_curv    (theta, r, x, z)
        Gamma    = self._spherical_Gamma   (theta, r, x, z)
        trK      = self._spherical_trK     (theta, r, x, z)

        dmetric            = self._spherical_dmetric(theta, r, x, z, metric, Gamma)
        dmetric_u          = self._spherical_dmetric_u(theta, r, x, z, dmetric, metric_u)
        metric_u_cart_yy   = self.metric_u_cart[1, 1].interp(x, z)
        dmetric_cart_yy_dz = self.dmetric_cart[2, 1, 1].interp(x, z)
        # XXX: unused, what was this for?
        # dmetric_cart_yy_dx = self.dmetric_cart[0, 1, 1].interp(x, z)

        u2 = metric_u[1, 1] * dr * dr - 2 * metric_u[0, 1] * dr + metric_u[0, 0]
        u  = np.sqrt(u2)
        u3 = u * u2

        du_F    = np.zeros((3,) + u.shape)
        du_F[0] = metric_u[0, 0] - metric_u[0, 1] * dr
        du_F[1] = metric_u[0, 1] - metric_u[1, 1] * dr

        # F_term = (∂^i F)(∂^j F)(Γ_{ij}^k ∂_k F + u K_{ij})
        F_term = np.zeros_like(u)
        for i, j in it.product(range(2), repeat = 2):
            F_term += du_F[i] * du_F[j] * (Gamma[0, i, j] - dr * Gamma[1, i, j] + u * curv[i, j])

        # X_term = γ^{ij} Γ_{ij}^k ∂_k F
        # this is the only term that is singular at θ = nπ
        X_term = np.zeros_like(u)
        for i, j in it.product(range(2), repeat = 2):
            X_term += metric_u[i, j] * (Gamma[0, i, j] - dr * Gamma[1, i, j])

        # γ^{φφ} Γ_{φφ}^r ∂_r F
        X_term_phi_reg_r  = metric_u[2, 2] * Gamma[0, 2, 2]
        X_term_phi_sing_r = -0.5 * (
                metric_u[0, 0] / r * (2. + r *
                    metric_u_cart_yy *  dmetric_cart_yy_dz) +
                dmetric_u[1, 0, 1] * 2
                )
        # XXX: what is this?
        #X_term_phi_sing_r = -dmetric_u[1, 0, 1] - metric_u[0, 1] / metric_cart

        # γ^{φφ} Γ_{φφ}^θ
        # at the axis this term goes to γ^{θθ} * d2r, i.e. the rhs we are computing;
        # when multiplied by prefactors they all cancel out, so we get:
        #  rhs = <regular part> - rhs
        # in other words, at the axis we need to set X_term_phi_theta to 0 and
        # instead divide rhs by 2
        X_term_phi_reg_theta  = -metric_u[2, 2] * Gamma[1, 2, 2] * dr

        X_term += np.where(axis,
                           X_term_phi_sing_r + 0.0,
                           X_term_phi_reg_r + X_term_phi_reg_theta)

        denom = metric_u[0, 0] * metric_u[1, 1] - metric_u[0, 1] * metric_u[0, 1]
        ret   = -(u2 * X_term + u3 * trK - F_term + np.sqrt(2.0) * val * u3) / denom

        ret  *= np.where(axis, 0.5, 1.0)

        return ret

    def _ah_rhs_cart(self, l, H, expansion_val = 0.0, clockwise = True,
                     normalize_u = True):
        """
        Right-hand side of the apparent-horizon ODE in the Cartesian
        parametrization (eq. (4.19) in thesis).
        """
        x, z, u_x, u_z = H

        bound_points = self._interp_order // 2 + 1
        bound_x = self._X[0,             -bound_points]
        bound_z = self._Z[-bound_points, 0            ]

        if ((not np.all(np.isfinite(x))) or (not np.all(np.isfinite(z))) or
            np.any(x > bound_x) or np.any(x < -bound_x) or
            np.any(z > bound_z) or np.any(z < -bound_z)):
            return [u_x, u_z, np.array([0.0]), np.array([0.0])]

        # interpolate the metric components
        metric    = self.metric_cart.interp   (x, z)
        dmetric   = self.dmetric_cart.interp  (x, z)
        metric_u  = self.metric_u_cart.interp (x, z)
        dmetric_u = self.dmetric_u_cart.interp(x, z)
        Gamma     = self.Gamma_cart.interp    (x, z)
        curv      = self.curv_cart.interp     (x, z)

        detg      = utils.matrix_det(metric)

        x2        = x * x
        R2        = x2 * metric[1, 1]

        # tangent vector to the surface
        u = np.zeros((3,) + x.shape)
        u[0] = u_x
        u[2] = u_z

        if normalize_u:
            u2 = np.einsum('ij...,i...,j...', metric, u, u)
            u /= np.sqrt(u2)
            u_x = u[0]
            u_z = u[2]

        # angular Killing vector
        eta = np.zeros((3,) + x.shape)
        eta[1] = x

        sign = 1.0 if clockwise else -1.0

        s = np.zeros((3,) + x.shape)
        s[0] = -u_z
        s[2] = u_x
        s *= sign * np.sqrt(detg) * np.sqrt(metric_u[1, 1])

        su = np.einsum('ij...,j...->i...', metric_u, s)

        mu = np.empty_like(metric_u)
        for i, j in it.product(range(3), repeat = 2):
            mu[i, j] = metric_u[i, j] - su[i] * su[j]

        term1_reg  = (su[0] * dmetric[0, 1, 1] + su[2] * dmetric[2, 1, 1]) / (2.0 * metric[1, 1])
        term1_sing = np.where(np.abs(x) > 1e-12,
                              su[0] / x,
                              s[2] * dmetric_u[0, 0, 2])

        term2 = np.einsum('ij...,ij...', mu, curv)

        val = term1_reg + term1_sing - term2 - np.sqrt(2.0) * expansion_val

        d2x = su[0] * val - np.einsum('ij...,i...,j...', Gamma[0], u, u)
        d2z = su[2] * val - np.einsum('ij...,i...,j...', Gamma[2], u, u)

        d2z *= np.where(np.abs(x) > 1e-13, 1.0,
                1.0 / (1. + su[2] * sign * metric_u[0, 0] * np.sqrt(detg * metric_u[1, 1]) / u_x))

        return np.array([u_x, u_z, d2x, d2z])

    def _dF_r_fd(self, theta, H):
        eps = 1e-8
        r, R = H

        rp = r + eps
        rm = r - eps
        Fp = self._ah_rhs(theta, (rp, R))
        Fm = self._ah_rhs(theta, (rm, R))
        return (Fp - Fm) / (2 * eps)

    def _dF_r_exact(self, theta, H):
        r, dr = H

        x = r * np.sin(theta)
        z = r * np.cos(theta)

        metric    = self._spherical_metric   (theta, r, x, z)
        metric_u  = self._spherical_metric_u (theta, r, x, z)
        curv      = self._spherical_curv     (theta, r, x, z)
        dcurv     = self._spherical_dcurv    (theta, r, x, z)
        Gamma     = self._spherical_Gamma    (theta, r, x, z)
        dmetric   = self._spherical_dmetric  (theta, r, x, z, metric, Gamma)
        dmetric_u = self._spherical_dmetric_u(theta, r, x, z, dmetric, metric_u)
        dGamma    = self._spherical_dGamma   (theta, r, x, z)

        u2     = metric_u[1, 1] * dr * dr - 2 * metric_u[0, 1] * dr + metric_u[0, 0]
        u      = np.sqrt(u2)
        var_u2 = dmetric_u[0, 1, 1] * dr * dr - 2 * dmetric_u[0, 0, 1] * dr + dmetric_u[0, 0, 0]
        var_u  = var_u2 / (2 * u)

        df_ij     = np.empty_like(metric_u)
        var_df_ij = np.empty_like(metric_u)
        for i, j in it.product(range(3), repeat = 2):
            df_ij[i, j]     =  ( metric_u   [0, i] -  metric_u   [1, i] * dr) * (metric_u[0, j] - metric_u[1, j] * dr)
            var_df_ij[i, j] = ((dmetric_u[0, 0, i] - dmetric_u[0, 1, i] * dr) * (metric_u[0, j] - metric_u[1, j] * dr) +
                               (dmetric_u[0, 0, j] - dmetric_u[0, 1, j] * dr) * (metric_u[0, i] - metric_u[1, i] * dr))

        term1     = u2 * metric_u                         - df_ij
        var_term1 = var_u2 * metric_u + u2 * dmetric_u[0] - var_df_ij
        term2     = Gamma[0] - dr * Gamma[1] + u * curv
        var_term2 = dGamma[0, 0] - dr * dGamma[0, 1] + dcurv[0] * u + curv * var_u

        denom     =  metric_u   [0, 0] * metric_u[1, 1]                                       -      metric_u   [0, 1] * metric_u[0, 1]
        var_denom = dmetric_u[0, 0, 0] * metric_u[1, 1] + metric_u[0, 0] * dmetric_u[0, 1, 1] - 2 * dmetric_u[0, 0, 1] * metric_u[0, 1]

        ret = -(np.einsum('ij...,ij...', var_term1, term2) + np.einsum('ij...,ij...', term1, var_term2) - np.einsum('ij...,ij...', term1, term2) * var_denom / denom) / denom
        return ret

    def _dF_R_fd(self, theta, H):
        eps = 1e-8
        r, R = H

        Rp = R + eps
        Rm = R - eps
        Fp = self._ah_rhs(theta, (r, Rp))
        Fm = self._ah_rhs(theta, (r, Rm))
        return (Fp - Fm) / (2 * eps)

    def _dF_R_exact(self, theta, H):
        r, dr = H

        x = r * np.sin(theta)
        z = r * np.cos(theta)

        metric_u = self._spherical_metric_u(theta, r, x, z)
        curv     = self._spherical_curv    (theta, r, x, z)
        Gamma    = self._spherical_Gamma   (theta, r, x, z)

        u2     = metric_u[1, 1] * dr * dr - 2 * metric_u[0, 1] * dr + metric_u[0, 0]
        u      = np.sqrt(u2)
        var_u2 = metric_u[1, 1] * dr * 2  - 2 * metric_u[0, 1]
        var_u  = var_u2 / (2 * u)

        df_ij     = np.empty_like(metric_u)
        var_df_ij = np.empty_like(metric_u)

        for i, j in it.product(range(3), repeat = 2):
            df_ij[i, j]     = (metric_u[0, i] - metric_u[1, i] * dr) * (metric_u[0, j] - metric_u[1, j] * dr)
            var_df_ij[i, j] = -metric_u[1, i] * (metric_u[0, j] - metric_u[1, j] * dr) - metric_u[1, j] * (metric_u[0, i] - metric_u[1, i] * dr)

        term1     = u2 * metric_u - df_ij
        var_term1 = var_u2 * metric_u - var_df_ij
        term2     = Gamma[0] - dr * Gamma[1] + u * curv
        var_term2 = -Gamma[1] + curv * var_u

        ret = -(np.einsum('ij...,ij...', var_term1, term2) + np.einsum('ij...,ij...', term1, var_term2)) / (metric_u[0, 0] * metric_u[1, 1] - metric_u[0, 1] * metric_u[0, 1])
        return ret

    def hor_area(self, hor, log2points = 10):
        theta = np.linspace(0, np.pi, (1 << log2points) + 1)
        r     = hor.eval(theta)
        dr    = hor.eval(theta, 1)

        st    = np.sin(theta)
        ct    = np.cos(theta)

        x     = r * st
        z     = r * ct

        r2    = r * r
        z2    = z * z

        metric_u = self.metric_u_cart.interp(x, z)

        metric_u_det = utils.matrix_det(metric_u)

        ds = np.empty((3,) + theta.shape)
        ds[0] = x / r - dr * x * z / (r2 * np.sqrt(r2 - z2))
        ds[1] = 0.0
        ds[2] = z / r + dr * np.sqrt(r2 - z2) / r2
        lm2 = np.einsum('ij...,i...,j...', metric_u, ds, ds)

        dA = np.sqrt(lm2 / metric_u_det) * r2 * st
        dA[0]  = 0.0
        dA[-1] = 0.0

        return scipy.integrate.romb(dA, theta[1] - theta[0]) * 2. * np.pi

    def hor_mass(self, hor, *args, **kwargs):
        A = self.hor_area(hor, *args, **kwargs)
        return np.sqrt(A / (16. * np.pi))

    def calc_expansion(self, surf, direction):
        """
        Calculate expansion of null geodesics on a sequence of surfaces [1]. The
        surfaces are specified as level surfaces of F(r, θ) = r - h(θ).

        [1] Alcubierre (2008): Introduction to 3+1 Numerical Relativity, chapter
            6.7, specifically equation (6.7.13).

        :param callable surf: A callable that specifies the surfaces. Accepts an
                              array of θ and returns the array of correponding h.
        :param int direction: Values of 1/-1 specify that the expansion of outgoing
                              or ingoing geodesics is to be computed.
        :rtype: array_like, shape (self.Z.shape[0], self.X.shape[0])
        :return: Expansion values at the grid formed from X and Z.
        """
        X, Z = self.X, self.Z
        dX = [X[0, 1] - X[0, 0], 0, Z[1, 0] - Z[0, 0]]

        R     = np.sqrt(X ** 2 + Z ** 2)
        Theta = np.where(R > 1e-12, np.arccos(Z / R), 0.0)

        trK = np.einsum('ij...,ij...', self.metric_u_cart, self.curv_cart)

        F = R[:]
        for i in range(Theta.shape[0]):
            F[i] -= surf.eval(Theta[i])

        dF = np.empty((3,) + F.shape)
        dF[0] = self._diff_op(F, 1, dX[0])
        dF[1] = 0.0
        dF[2] = self._diff_op(F, 0, dX[2])

        s_l  = direction * dF[:]
        s_u  = np.einsum('ij...,j...->i...', self.metric_u_cart, s_l)
        s_u /= np.sqrt(np.einsum('ij...,i...,j...', self.metric_cart, s_u, s_u))

        ds_u = np.zeros((3,) + s_u.shape)
        for i in range(3):
            for j in range(3):
                if i == 1 or j == 1:
                    continue
                diff_dir = 1 if (i == 0) else 0
                ds_u[i, j] = self._diff_op(s_u[j], diff_dir, dX[i])
        ds_u[1, 1] = np.where(np.abs(X) > 1e-8, s_u[0] / X, ds_u[0, 0])

        Div_s_u = np.einsum('ii...', ds_u) + np.einsum('iki...,k...', self.Gamma_cart, s_u)

        H = Div_s_u - trK + np.einsum('ij...,i...,j...', self.curv_cart, s_u, s_u)

        return H

    def shoot_cart(self, z0, *,
                   expansion_val = 0.0, clockwise = True,
                   atol = 1e-8, rtol = 1e-8,
                   bound_fact = 0.85, axis_tol = 1e-8,
                   lambda_bound = 1024.0,
                   terminate_axis_repel = False, axis_approach_fact = 5e-2):
        """
        Shoot a trial curve for an trapped surface (or a constant-expansion
        surface, with expansion_val != 0) in cartesian parametrization.

        :param float z0: Point on the axis of symmetry from which to shoot the
                         curve.

        :param float expansion_val: Integrate for the surface with this value of
                                    expansion. Should be 0 for a trapped
                                    surface.

        :param bool clockwise: When True, integrate clockwise, i.e. initial
                               normal vector points into positive-z direction.
                               Otherwise initial normal vector points in the
                               negative-z direction and integration is
                               counter-clockwise.

        :param float bound_fact: Number between 0 and 1 indicating how close to
                                 the outer boundary should the curve get before
                                 terminating with ShootResult.BOUNDARY.

        :param float axis_tol: The value of the x coordinate for which the
                               integration is assumed to have hit the axis and
                               will terminate with ShootResult.AXIS.

        :param float lambda_bound: Maximum value of the curve parameter λ, when
                                   reached the integration will terminate with
                                   ShootResult.EXPIRED.

        :param bool terminate_axis_repel: When True, integration will be
                                          terminated with ShootResult.AXIS_REPEL
                                          when the curve is "repelled" by the
                                          axis.

        :param float axis_approach_fact: When terminate_axis_repel=True, the
                                         curve must be below
                                         axis_approach_fact *
                                         "maximum value of the x coordinate
                                          reached by the curve so far"
                                         to be considered "repelled".
        """
        # normalized tangent vector
        u = np.zeros((3,))
        u[0] = 1.0

        metric = self.metric_cart.interp(0.0, z0)
        u2 = np.einsum('ij...,i...,j...', metric, u, u)
        u /= np.sqrt(u2)

        # ODE initial value
        y0 = (0.0, z0, u[0], u[2])

        rhs = lambda t, y: self._ah_rhs_cart(t, y,
                                             expansion_val = expansion_val,
                                             clockwise = clockwise)

        solver = scipy.integrate.LSODA(rhs, y0 = y0, t0 = 0.0, t_bound = lambda_bound,
                                       rtol = rtol, atol = atol, vectorized = True)

        ts           = [solver.t]
        interpolants = []

        status = None
        y_prev = solver.y
        max_x  = 0.0
        while solver.t < solver.t_bound:
            msg = solver.step()
            if solver.status == 'failed':
                raise IntegrationFailed('Integration failed at λ=%g y=%s: %s' %
                                        (solver.t, solver.y, msg))
            elif solver.status == 'finished':
                status = ShootResult.EXPIRED
                break

            x, z, u_x, u_z = solver.y

            ts.append(solver.t)
            interpolants.append(solver.dense_output())

            # getting too close to the boundary
            if (abs(x) > self._X[0, -1] * bound_fact or
                abs(z) > self._Z[-1, 0] * bound_fact):
                status = ShootResult.BOUNDARY
                break

            # got very close to the axis and moving closer
            if abs(x) < axis_tol and np.sign(u_x) != np.sign(x):
                status = ShootResult.AXIS
                break

            # integration got repelled by the axis
            if (terminate_axis_repel and
                # u^x changed sign
                y_prev[2] <= 0 and u_x >= 0 and
                # we either looped inward, or are sufficiently close to the axis
                (u_z > 0 or x < axis_approach_fact * max_x)):
                status = ShootResult.AXIS_REPEL
                break

            y_prev = solver.y
            max_x  = max(max_x, y_prev[0])

        # gather all the integration segments into a single solution object
        return ShootSolution(status, scipy.integrate.OdeSolution(ts, interpolants))

    def _horizon_weight(self, z0, *,
                        expansion_val = 0.0,
                        verbose = False):
        ######################################################################
        # How this works:
        # - the z axis is repulsive (equation coefficients diverge to ∞ as x→0)
        #   for curves that are not perpendicular to the axis (z'≠0), i.e. only
        #   curves with z'=0 can hit the axis (in numerical approximation, z'
        #   has to be small enough);
        #
        # - when shooting from z=z0 that is exactly the horizon, the curve will
        #   go from x=0 to some maximum x value, then turn back towards the axis
        #   and eventually hit it, i.e. we will get an AXIS result;
        #
        # - when shooting from z=z0 slightly ABOVE the horizon, the curve will
        #   follow the horizon most of the way, but will eventually loop
        #   "downward" (toward negative z) and away from the axis, i.e. we will
        #   get a AXIS_REPEL result with u_x=0, u_z<0;
        #
        # - shooting from z=z0 even higher ABOVE the horizon, the curve may
        #   eventually hit the domain boundary without any loops
        #
        # - when shooting from z=z0 slightly BELOW the horizon, the curve will
        #   loop "upwards" instead, i.e. we will get an AXIS_REPEL result with
        #   u_x=0, u_z>0;
        #
        # So our algorithm looks for the zero of a function that is proportional
        # to some power (we pick 2, some other power might be better?) of the
        # final distance from the axis and is
        # - positive if the curve looped back on itself
        # - negative otherwise
        ######################################################################
        if verbose:
            sys.stderr.write('find_cart(): trying surface through z=%g\n' % z0)

        sol = self.shoot_cart(z0, expansion_val = expansion_val,
                              terminate_axis_repel = True)
        if sol.result in (ShootResult.EXPIRED, ShootResult.BOUNDARY):
            x   = sol.sol(sol.sol.t_max)[0]
            ret = -(x * x)

            if verbose:
                sys.stderr.write('find_cart(): hit boundary; final x=%g; '
                                 'weight=%g\n' % (x, ret))
            return ret
        if sol.result == ShootResult.AXIS:
            if verbose:
                sys.stderr.write('find_cart(): hit z-axis\n')
            return 0.0
        if sol.result == ShootResult.AXIS_REPEL:
            x, z, u_x, u_z = sol.sol(sol.sol.t_max)
            ret = -(x ** 2) * np.sign((z - z0) * u_z)
            if verbose:
                sys.stderr.write('find_cart(): axis repel x=%g z=%g u_x=%g u_z=%g weight=%g\n' %
                                 (x, z, u_x, u_z, ret))
            return ret

        raise RuntimeError('This should be unreachable')

    def horizon_cart(self, *,
                     expansion_val = 0.0, rtol = 1e-2, verbose = False,
                     bracket = None, trial_points = None, num_trial_points = 128,
                     trial_range = None):
        """
        Find an apparent horizon (actually a trapped surface, or more generally
        a surface of constant given expansion expansion_val), in cartesian
        parametrization.

        :param float expansion_val: Integrate for the surface with this value of
                                    expansion. Should be 0 for a trapped
                                    surface.

        :param pair of float bracket: When given, specifies the interval of z
                                      coordinates at which the horizon
                                      intersects the symmetry axis. When not
                                      given, the code tries to determine the
                                      bracket automatically.

        :param list of float trial_points: When bracket is not given, try to
                                           find it within this list of z
                                           coordinates.

        :param int num_trial_points: When trial_points is not given, generate
                                     this many trial points automatically.
                                     Defaults to 128.

        :param float trial_range: When trial_points is not given, generate
                                  num_trial_points point from z0=0 to
                                  trial_range. Will be guessed when not given.
        """

        # find the bracket if one is not supplied
        if bracket is None:
            if trial_points is None:
                if trial_range is None:
                    trial_range = 0.65 * self._Z[-1, 0]
                trial_points = np.linspace(trial_range, 0.0, num_trial_points)

            bracket = [None, trial_points[0]]
            for z0 in trial_points:
                if verbose:
                    sys.stderr.write('Trying surface through z=%g\n' % z0)

                surf = self.shoot_cart(z0, expansion_val = expansion_val,
                                       terminate_axis_repel = True)
                if (surf.result != ShootResult.AXIS_REPEL or
                    surf.sol(surf.sol.t_max)[3] < 0):
                    if verbose:
                        sys.stderr.write('No upwards turns at (z=%g)\n' % z0)
                    bracket[1] = z0
                    continue

                #ret = ahc.calc_expansion_surf(z, -0.1, tol = 1e-1)
                #if ret[0] > 0.0:
                #    sys.stderr.write('Upwards turn of the Θ=0 curve at (t=%g,z=%g), but expansion is positive\n' % (t, z))
                #    continue

                #if verbose:
                #    sys.stderr.write('Found negative expansion %g at (t=%g,z=%g)\n' % (ret[0], t, z))

                bracket[0] = z0
                break

            if bracket[0] is None:
                raise NoBracketFound

        if verbose:
            sys.stderr.write('Looking for root in z0=[%g -- %g]\n' %
                             (bracket[1], bracket[0]))

        hw = lambda z0: self._horizon_weight(z0, expansion_val = expansion_val,
                                             verbose = verbose)

        hor_z = root_scalar(hw, method = 'brentq',
                            bracket = bracket, rtol = rtol).root

        if verbose:
            sys.stderr.write('Found root: %g\n' % hor_z)

        return self.shoot_cart(hor_z, expansion_val = expansion_val,
                               terminate_axis_repel = True)

    def _expansion_weight(self, expansion_val, z0, *,
                          verbose = False):
        ######################################################################
        # How this works (first see the comment in horizon_cart() above):
        #
        # - for a large enough value of expansion, the curve will loop back on
        #   itself, i.e. it will go from the z-axis to some maximum x value, then
        #   turn back towards the axis and then loop "upwards" (i.e. x'=0,z'>0)
        #   in the limit of expansion→∞ it collapses to a point
        #
        # - the z axis is repulsive (equation coefficients diverge to ∞ as x→0)
        #   for curves that are not perpendicular to the axis (z'≠0), i.e. only
        #   curves with z'=0 can hit the axis (in numerical approximation, z'
        #   has to be small enough)
        #
        # - decreasing the expansion, the curve stops looping upwards and loops
        #   downwards instead (i.e. x'=0,z'<0)
        #
        # - between those two is the curve that hits the axis with exactly z'=0,
        #   which is our solution
        #
        # - decreasing the expansion even further, the curve may hit the domain
        #   boundary without any loops
        #
        # So our algorithm looks for the zero of a function that is proportional
        # to some power (we pick 2, some other power might be better?) of the
        # final distance from the axis and is
        # - positive if the curve looped back on itself
        # - negative otherwise
        ######################################################################
        if verbose:
            sys.stderr.write('expansion_surf_at_point(): shooting curve expansion=%g\n' % expansion_val)

        sol = self.shoot_cart(z0, expansion_val = expansion_val,
                              terminate_axis_repel = True)

        if sol.result in (ShootResult.EXPIRED, ShootResult.BOUNDARY):
            x = sol.sol(sol.sol.t_max)[0]

            if verbose:
                sys.stderr.write('expansion_surf_at_point(): hit boundary; final x=%g\n' % x)

            return -(x ** 2)

        if sol.result == ShootResult.AXIS:
            if verbose:
                sys.stderr.write('expansion_surf_at_point(): hit z-axis\n')

            return 0.0

        if sol.result == ShootResult.AXIS_REPEL:
            x, z, u_x, u_z = sol.sol(sol.sol.t_max)
            ret = -(x ** 2) * np.sign((z - z0) * u_z)

            if verbose:
                sys.stderr.write('expansion_surf_at_point(): '
                                 'axis repel x=%g z=%g u_x=%g u_z=%g weight=%g\n' %
                                 (x, z, u_x, u_z, ret))

            return ret

        raise RuntimeError('This should be unreachable')

    def expansion_surf_at_point(self, z0, *,
                                initial_guess = 0.0, tol = 1e-3, verbose = False):
        """
        For a given point z0 on the z-axis, compute the curve of constant
        expansion that passes through it, and the value of that expansion.
        """

        ew = lambda H: self._expansion_weight(H, z0, verbose = verbose)

        # try the initial guess
        if verbose:
            sys.stderr.write('expansion_surf_at_point(): looking for bracket\n')
        val0 = initial_guess
        w0   = ew(val0)

        # look for a bracket,
        # unless we just happened to hit zero on the first try,
        if w0 != 0.0:
            direction = -np.sign(w0)

            for offset in np.linspace(0.1, 3.0, 32):
                val1 = initial_guess + direction * (offset ** 3)
                w1   = ew(val1)
                if w0 * w1 <= 0.0:
                    break
                else:
                    # sign didn't change, update old side of the bracket
                    w0   = w1
                    val0 = val1

            if w1 * w0 > 0.0:
                raise NoBracketFound

            bracket = [val0, val1]

            if verbose:
                sys.stderr.write('expansion_surf_at_point(): got bracket: %s\n' % str(bracket))

            root = root_scalar(ew, method = 'brentq', bracket = bracket, xtol = tol).root
        else:
            root = val0

        if verbose:
            sys.stderr.write('expansion_surf_at_point(): found surface for z=%g: expansion=%g\n' % (z0, root))

        sol = self.shoot_cart(z0, expansion_val = root, terminate_axis_repel = True)
        return root, sol
