import numpy             as np
import scipy.integrate   as integrate

def dist_proper(coord, gXX):
    """
    Calculate proper distance along given spatial axis.

    :param array_like coord: 1D array of spatial coordinates.
    :param array_like gXX: Values of the XX component of the spatial metric at
                           provided coordinates.
    :return: array of proper distances from coord[0].
    """
    return integrate.cumulative_trapezoid(np.sqrt(gXX), coord, initial = 0.0)
