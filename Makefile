TARGET = lib_interp_c.so

CFLAGS = -std=c99 -fPIC -O3 -g
LDFLAGS = -shared
CC = cc

OBJS = _interp_c.o

$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS) ${LDFLAGS}

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

test: $(TARGET)
	LD_LIBRARY_PATH=.:$$LD_LIBRARY_PATH python3 -m unittest

clean:
	-rm -f *.o *.d $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: clean test
