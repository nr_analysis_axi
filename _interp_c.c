#include <math.h>

#define STENCIL 2
#include "_interp_c_template.c"
#undef STENCIL

#define STENCIL 4
#include "_interp_c_template.c"
#undef STENCIL

#define STENCIL 6
#include "_interp_c_template.c"
#undef STENCIL

#define STENCIL 8
#include "_interp_c_template.c"
#undef STENCIL

#define STENCIL 10
#include "_interp_c_template.c"
#undef STENCIL
