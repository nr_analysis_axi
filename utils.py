import warnings
warnings.warn('This module is deprecated, use math_utils.array_utils', DeprecationWarning)

from math_utils.array_utils import matrix_invert, matrix_det, array_reflect
